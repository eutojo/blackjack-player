----------------------------------------------------------------------------------
-- Company: UNSW
-- Engineer: Jorgen Peddersen
-- 
-- Create Date:    16:06:48 09/26/2006 
-- Design Name:    Blackjack Player
-- Module Name:    Blackjack - Structural 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity Blackjack is
    Port ( clk : in  STD_LOGIC;     -- on-board clock - fast
           sw_clk : in STD_LOGIC;   -- switch controlled clock - slow
           start : in  STD_LOGIC;   -- start input (asynchronous reset)
           cardValue : in  STD_LOGIC_VECTOR (3 downto 0);
           cardReady : in  STD_LOGIC;
           newCard : out  STD_LOGIC;
           lost : out  STD_LOGIC;
           finished : out  STD_LOGIC;
           score : out  STD_LOGIC_VECTOR (4 downto 0);
           data7s : out  STD_LOGIC_VECTOR (0 to 7);
           addr7s : out  STD_LOGIC_VECTOR (3 downto 0));
end Blackjack;

architecture Structural of Blackjack is

  component Blackjack_FSM
    port (
      clk                       : in  std_logic;
      rst                       : in  std_logic;
      cardReady                 : in  std_logic;
      newCard                   : out std_logic;
      lost                      : out std_logic;
      finished                  : out std_logic;
      cmp11, cmp16, cmp21       : in  std_logic;
      sel                       : out std_logic;
      enaLoad, enaAdd, enaScore : out std_logic);
  end component;

  component Blackjack_DataPath
    port (
      clk                       : in  std_logic;
      rst                       : in  std_logic;
      cardValue                 : in  std_logic_vector(3 downto 0);
      score                     : buffer std_logic_vector(4 downto 0);
      sel                       : in  std_logic;
      enaLoad, enaAdd, enaScore : in  std_logic;
      cmp11, cmp16, cmp21       : out std_logic);
  end component;

  signal sel                       : std_logic;
  signal enaLoad, enaAdd, enaScore : std_logic;
  signal cmp11, cmp16, cmp21       : std_logic;

  signal score_sig : std_logic_vector(4 downto 0);
  
  -- The following signals are used to synchronise the inputs to the clock
  signal cardReady_sync, cardReady_prev : std_logic;
  signal cardValue_sync, cardValue_prev : std_logic_vector(3 downto 0);
  signal sync_count : std_logic_vector(19 downto 0);
  
begin

  -- purpose: This process debounces the input switches and synchronises
  --          to the positive edge of clk.  You may comment this process
  --          out if you want to use sw_clk, but you don't have to.
  -- type   : sequential
  -- inputs : clk, rst, cardReady, cardValue
  -- outputs: cardReady_sync, cardValue_sync
--  synchronise: process (clk, start)
--  begin  -- process synchronise
--    if start = '1' then                   -- asynchronous reset (active high)
--      cardReady_sync <= '0';
--      cardReady_prev <= '0';
--      cardValue_sync <= (others => '0');
--      cardValue_prev <= (others => '0');
--      sync_count <= (others => '0');
--    elsif clk'event and clk = '1' then  -- rising clock edge
--      cardReady_prev <= cardReady;
--      cardValue_prev <= cardValue;
--		
--      -- The following counter counts time that the inputs have been steady.
--      -- At 50Mhz, the signal must be steady for approx. 10 milliseconds.
--      if cardReady /= cardReady_prev or cardValue /= cardValue_prev then
--        sync_count <= (others => '0');
--      elsif sync_count /= x"FFFFF" then
--        sync_count <= sync_count + 1;
--      end if;
--		
--      -- If the full time is reached, update the signals.
--      if sync_count = x"FFFFF" then
--        cardReady_sync <= cardReady_prev;
--        cardValue_sync <= cardValue_prev;
--      end if;  
--
--    end if;
--  end process synchronise;

  -- The following two instantiations are set up for the normal clock.
  -- To set up for sw_clk, uncomment the commented lines and comment out
  -- the lines above each commented line.
  BJ_FSM: Blackjack_FSM
    port map (
 --       clk       => clk,
      clk       => sw_clk,
        rst       => start,
 --       cardReady => cardReady_sync,
      cardReady => cardReady,
        newCard   => newCard,
        lost      => lost,
        finished  => finished,
        cmp11     => cmp11,
        cmp16     => cmp16,
        cmp21     => cmp21,
        sel       => sel,
        enaLoad   => enaLoad,
        enaAdd    => enaAdd,
        enaScore  => enaScore);

  BJ_DP: Blackjack_DataPath
    port map (
--        clk       => clk,
      clk       => sw_clk,
        rst       => start,
--        cardValue => cardValue_sync,
      cardValue => cardValue,
        score     => score_sig,
        sel       => sel,
        enaLoad   => enaLoad,
        enaAdd    => enaAdd,
        enaScore  => enaScore,
        cmp11     => cmp11,
        cmp16     => cmp16,
        cmp21     => cmp21);

  -- 7 Segment Display of score is OPTIONAL.  Comment the following
  -- instantiation if you don't want to use it.
  -- BJ_7seg: Blackjack_7Seg
  --  port map (
  --      clk   => clk,
  --      rst   => start,
  --      score => score_sig,
  --      data  => data7s,
  --      addr  => addr7s);
  --
  -- Do not comment the following line.  A signal is needed as score is used as
  -- an input to the 7 Segment Display AND an output of the entity.
  score <= score_sig;
 --score <= "10000";
end Structural;

----------------------------------------------------------------------------------
-- Company: UNSW
-- Engineer: Jorgen Peddersen
-- 
-- Create Date:    16:06:48 09/26/2006 
-- Design Name:    Blackjack Player
-- Module Name:    Blackjack Datapath - Structural 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Blackjack_DataPath is
  
  port (
    clk                       : in  std_logic;
    rst                       : in  std_logic;
    cardValue                 : in  std_logic_vector(3 downto 0);
    score                     : buffer std_logic_vector(4 downto 0);
    sel                       : in  std_logic;
    enaLoad, enaAdd, enaScore : in  std_logic;
    cmp11, cmp16, cmp21       : out std_logic);

end Blackjack_DataPath;

architecture Structural of Blackjack_DataPath is
	SIGNAL receivedCardValue: std_logic_vector(3 downto 0);
	SIGNAL additionResult : std_logic_vector(4 downto 0);
begin
	
	cmp16 <= '1' WHEN (additionResult > 16) ELSE '0';
	cmp21 <= '1' WHEN (additionResult > 21) ELSE '0';
	
PROCESS(rst, clk)
BEGIN

	IF rst = '0' THEN
		score <= (OTHERS => '0');
	ELSIF clk'EVENT AND clk = '1' THEN
		IF enaLoad = '1' THEN
			receivedCardValue <= cardValue;
		ELSE
			receivedCardValue <= receivedCardValue;
		END IF;
		
		IF enaAdd = '1' THEN
			additionResult <= receivedCardValue + score;
		ELSE 
			additionResult <= additionResult;
		END IF;
		
		IF enaScore = '1' THEN
			score <= additionResult;
--			IF score > 21 THEN
--				cmp21 <= '1';
--			END IF;
--			IF score > 16 THEN
--				cmp16 <= '1';
--			END IF;
		ELSE
			score <= score;
		END IF;
	END IF;
END PROCESS;

  

end Structural;

----------------------------------------------------------------------------------

-- Company: UNSW
-- Engineer: Jorgen Peddersen
-- 
-- Create Date:    16:06:48 09/26/2006 
-- Design Name:    Blackjack Player
-- Module Name:    Blackjack FSM - Behavioural 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Blackjack_FSM is
  
  port (
    clk                       : in  std_logic;
    rst                       : in  std_logic;
    cardReady                 : in  std_logic;
    newCard                   : out std_logic;
    lost                      : out std_logic;
    finished                  : out std_logic;
    cmp11, cmp16, cmp21       : in  std_logic;
    sel                       : out std_logic;
    enaLoad, enaAdd, enaScore : out std_logic);

end Blackjack_FSM;


architecture Behavioural of Blackjack_FSM is
	TYPE state_type IS (RESET, WAITING, LOAD, SCORE, DETERMINE, FINISH, BUST);
	ATTRIBUTE signal_encoding : string;
	ATTRIBUTE signal_encoding OF state_type : 
		TYPE IS "000 001 010 011 100 101 110 111";
	SIGNAL current_state, next_state: state_type;
begin  -- Behavioural

	PROCESS(rst, clk)
	BEGIN
		IF rst = '0' THEN
			current_state <= RESET;
		ELSIF clk'EVENT and clk = '1' THEN
			CASE current_state IS
				WHEN RESET =>
					current_state <= WAITING;
				WHEN WAITING =>
					IF cardReady = '1' THEN
						current_state <= LOAD;
					ELSE
						current_state <= WAITING;
					END IF;
				WHEN LOAD =>
					IF cardReady = '1' THEN
						current_state <= LOAD;
					ELSE
						current_state <= SCORE;
					END IF;
				WHEN SCORE =>
					current_state <= DETERMINE;
				WHEN DETERMINE =>
					IF cmp21 = '1' THEN
						current_state <= BUST;
					ELSIF cmp16 = '1' THEN
						current_state <= FINISH;
					ELSE
						current_state <= WAITING;
					END IF;
				WHEN FINISH =>
				WHEN BUST =>
			END CASE;
		END IF;
	END PROCESS;
	
	
	FSM_outputs: PROCESS(current_state, cardReady)
	BEGIN
		newCard <= '0';
		enaLoad <= '0';
		enaScore <= '0';
		finished <= '0';
		lost <= '0';
		CASE current_state IS
			WHEN RESET =>
				newCard <= '1';
			WHEN WAITING =>
				IF cardReady = '1' THEN
					enaLoad <= '1';
				ELSE
					newCard <= '1';
				END IF;
			WHEN LOAD =>
				IF cardReady /= '1' THEN
					enaAdd <= '1';
				END IF;
			WHEN SCORE =>
				enaScore <= '1';
			WHEN DETERMINE =>
			WHEN FINISH =>
				finished <= '1';
			WHEN BUST =>
				lost <= '1';
		END CASE;
	END PROCESS;
	  
end Behavioural;

