library verilog;
use verilog.vl_types.all;
entity Blackjack_vlg_check_tst is
    port(
        addr7s          : in     vl_logic_vector(3 downto 0);
        data7s          : in     vl_logic_vector(0 to 7);
        finished        : in     vl_logic;
        lost            : in     vl_logic;
        newCard         : in     vl_logic;
        score           : in     vl_logic_vector(4 downto 0);
        sampler_rx      : in     vl_logic
    );
end Blackjack_vlg_check_tst;
