onerror {quit -f}
vlib work
vlog -work work blackjack1.vo
vlog -work work blackjack1.vt
vsim -novopt -c -t 1ps -L cycloneii_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate work.Blackjack_vlg_vec_tst
vcd file -direction blackjack1.msim.vcd
vcd add -internal Blackjack_vlg_vec_tst/*
vcd add -internal Blackjack_vlg_vec_tst/i1/*
add wave /*
run -all
